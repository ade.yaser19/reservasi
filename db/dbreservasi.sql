-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2023 at 10:41 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbreservasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `kode_booking` varchar(20) NOT NULL,
  `nama` varchar(300) NOT NULL,
  `telepon` varchar(16) NOT NULL,
  `reguler` int(3) NOT NULL,
  `senior` int(3) NOT NULL,
  `kids` int(3) NOT NULL,
  `tanggal` date NOT NULL,
  `jam` varchar(8) NOT NULL,
  `Status` varchar(100) NOT NULL,
  `id_meja_kursi` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `title`, `icon`, `url`, `is_active`) VALUES
(1, 'Dashboard', 'fa fa-tachometer', 'dashboard', 1),
(2, 'Pengguna', 'fa fa-users', 'users', 1),
(4, 'Menu', 'fa fa-navicon', 'menu', 1),
(5, 'Pengaturan', 'fa fa-gear', 'pengaturan', 1),
(6, 'Role', 'fa fa-shield', 'role', 1),
(7, 'Role Menu', 'fa fa-address-card', 'access', 1),
(8, 'Pesanan', 'glyphicon glyphicon-list-alt', 'pesanan', 0),
(9, 'Laporan', 'glyphicon glyphicon-print', 'report', 0),
(10, 'Jadwal', 'glyphicon glyphicon-calendar', 'jadwal', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mst_meja_kursi`
--

CREATE TABLE `mst_meja_kursi` (
  `id_meja_kursi` varchar(8) NOT NULL,
  `kapasitas_minimum` int(11) NOT NULL,
  `kapasitas_makasimum` int(11) NOT NULL,
  `Status` varchar(1) NOT NULL,
  `priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_meja_kursi`
--

INSERT INTO `mst_meja_kursi` (`id_meja_kursi`, `kapasitas_minimum`, `kapasitas_makasimum`, `Status`, `priority`) VALUES
('A1', 5, 6, 'A', 1),
('A10', 5, 6, 'A', 10),
('A2', 2, 4, 'A', 2),
('A3', 5, 6, 'A', 3),
('A4', 2, 4, 'A', 4),
('A5', 2, 4, 'A', 5),
('A6', 5, 6, 'A', 6),
('A7', 2, 4, 'A', 7),
('A8', 5, 6, 'A', 8),
('A9', 2, 4, 'A', 9);

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `nama_sistem` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `nama_sistem`) VALUES
(1, 'Fogo  GGP');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'Admin'),
(2, 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `ttm_memo`
--

CREATE TABLE `ttm_memo` (
  `id` int(20) NOT NULL,
  `no_booking` varchar(30) NOT NULL,
  `Catatan` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `no_telepon` varchar(16) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `img` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_pelanggan` int(11) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `password_hint` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `username`, `nama`, `no_telepon`, `email`, `password`, `img`, `date_created`, `id_pelanggan`, `alamat`, `password_hint`) VALUES
(4, 1, 'admin', 'admin', '085923132315', 'admin@admin.com', '$2y$10$wKPD3q3p4zhQrISUcHfRTeSLPQY9VyvQxilWMTxEG9MzzessAFK9O', 'images.png', '2023-07-23 17:00:00', 0, 'Bekasi', 'admin'),
(10, 2, 'fani', 'fani M', '085923132315', 'fani', '$2y$10$NGGO9NtAar./xfEIkMPjMuLiUoGufR8lHcQdJ3sYDZvvKcVmgx.ei', 'default.png', '0000-00-00 00:00:00', 0, '', 'bsihalohalo');

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(3, 1, 4),
(4, 3, 1),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(11, 1, 2),
(20, 2, 1),
(21, 1, 13),
(22, 2, 13),
(23, 1, 14),
(24, 2, 14),
(27, 1, 15),
(28, 1, 16),
(29, 1, 17),
(30, 1, 18),
(32, 1, 9),
(33, 1, 8),
(34, 1, 12),
(35, 3, 12),
(37, 3, 8),
(38, 1, 11),
(39, 1, 10),
(40, 2, 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`kode_booking`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_meja_kursi`
--
ALTER TABLE `mst_meja_kursi`
  ADD PRIMARY KEY (`id_meja_kursi`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ttm_memo`
--
ALTER TABLE `ttm_memo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ttm_memo`
--
ALTER TABLE `ttm_memo`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
