<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report_model extends CI_Model
{
    public function reporting_view($date)
    {
       $this->db->where('tanggal =', $date);
       return $this->db->get('jadwal')->result_array();
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->delete('role', ['id' => $id]);
    }
}
