<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Model
{
    public function insert_jadwal($data)
    {
        return $this->db->insert('jadwal', $data);
    }

    public function data_reservasi($data){
        $query = "select count(kode_booking)  as jumlah_booking from jadwal where tanggal ='$data' and status != 'Dibatalkan'";
        $query = $this->db->query($query);
        return $result = $query->result_array();

    }

    public function data_duduk($data){
        $query = "select count(kode_booking)  as jumlah_booking from jadwal where tanggal ='$data' and status = 'Duduk'";
        $query = $this->db->query($query);
        return $result = $query->result_array();

    }

    public function j_data_duduk($data){
        $query = "select coalesce(sum(reguler+senior+kids),0) as orang from jadwal where tanggal ='$data' and status = 'Duduk'";
        $query = $this->db->query($query);
        return $result = $query->result_array();

    }

    public function data_cancel($data){
        $query = "select count(kode_booking) as jumlah_booking 
        from jadwal where tanggal ='$data' and status = 'Dibatalkan'";
        $query = $this->db->query($query);
        return $result = $query->result_array();

    }

     public function data_orang($data){
        $query = "select sum(reguler+senior+kids) as orang from jadwal where tanggal ='$data' and status != 'Dibatalkan'";
        $query = $this->db->query($query);
        return $result = $query->result_array();
    } 

    public function update_jadwal($data,$no_booking){
       $this->db->where('kode_booking ', $no_booking);
       return $this->db->update('jadwal',$data);

    }

    public function save_memo($data){
     $this->db->insert('ttm_memo',$data);
         $str = $this->db->last_query();
   
    echo "<pre>";
    print_r($str);
    exit;

    }

    public function get_booking($date){
        $query = "select kode_booking from jadwal where tanggal ='$date' order by kode_booking";
        $query = $this->db->query($query);
        return $result = $query->result_array();
    }

    public function get_pass($username,$telp){
        $query = "select no_telepon,password_hint from users where username ='$username' and no_telepon='$telp'";
        $query = $this->db->query($query);
        return $result = $query->row_array();
    }

    function pelanggan_bokking(){
        $query = "select * from jadwal where id_meja_kursi ='' or id_meja_kursi is null order by kode_booking";
        $query = $this->db->query($query);
        return $result = $query->result_array();
    }

    function pelanggan_bokking_note($date){
        $query = "select * from jadwal where tanggal ='$date' order by kode_booking";
        $query = $this->db->query($query);
        return $result = $query->result_array();
    }

    public function data_jadwal($date){
        $query = "select mmk.id_meja_kursi,substr(j.jam,4,2),
                  CONCAT(mmk.kapasitas_minimum,' - ',mmk.kapasitas_makasimum) as orang,
                  j.tanggal,j.jam,j.nama,j.kode_booking,j.telepon ,j.Status,j.telepon,
                  case when substr(j.jam,1,1) =0 then CONCAT('jda',mmk.id_meja_kursi,'-',case when substr(j.jam,2,1)=0 then 1 else substr(j.jam,2,1) end,'-',case when substr(j.jam,4,2)<=60 and substr(j.jam,4,2)> 45 then '60' 
                  when  substr(j.jam,4,2)<= 45 and substr(j.jam,4,2)>=30 then '45' 
                  when  substr(j.jam,4,2)<= 30 and substr(j.jam,4,2)>=15 then '30' 
                  when  substr(j.jam,4,2)<= 15 and  substr(j.jam,4,2)>=0 then '15' end)
                  else CONCAT('jda',mmk.id_meja_kursi ,'-',substr(j.jam,1,2),'-',
                  case when substr(j.jam,4,2)> 45 then '60' 
                  when  substr(j.jam,4,2)<= 45 and substr(j.jam,4,2)>=30 then '45' 
                  when  substr(j.jam,4,2)<= 30 and substr(j.jam,4,2)>=15 then '30' 
                  when  substr(j.jam,4,2)>= 15 and  substr(j.jam,4,2)>=0 then '15' end)
                  end as id_urut
                  from mst_meja_kursi mmk  left join jadwal j on mmk.id_meja_kursi = j.id_meja_kursi
                  where j.tanggal ='$date'
                  order by mmk.priority asc;
            ";
        $query = $this->db->query($query);
        return $result = $query->result_array();
    }

    public function cek_kode_booking($kode){
        $query = "select count(*) as jumlah from jadwal where kode_booking ='$kode'";
        $query = $this->db->query($query);
        return $result = $query->row_array();
    }

    
}
