<!DOCTYPE html>
<html>
    <head>
        <!-- Site made with Mobirise Website Builder v5.8.14, https://mobirise.com -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="generator" content="Mobirise v5.8.14, mobirise.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="assets/images/fogo-brazilian-bbq-logo.jpg" type="image/x-icon">
        <meta name="description" content="">
        <title>Home</title>
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="assets/parallax/jarallax.css">
        <link rel="stylesheet" href="assets/dropdown/css/style.css">
        <link rel="stylesheet" href="assets/socicon/css/styles.css">
        <link rel="stylesheet" href="assets/theme/css/style.css">
        <link rel="preload" href="https://fonts.googleapis.com/css?family=Epilogue:100,200,300,400,500,600,700,800,900,100i,200i,300i,400i,500i,600i,700i,800i,900i&display=swap" as="style" onload="this.onload=null;this.rel='stylesheet'">
        <noscript>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Epilogue:100,200,300,400,500,600,700,800,900,100i,200i,300i,400i,500i,600i,700i,800i,900i&display=swap">
        </noscript>
        <link rel="preload" as="style" href="assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
    </head>
    <body>
        <section data-bs-version="5.1" class="menu menu1 cid-sFGzlAXw3z" once="menu" id="menu1-2">
            <nav class="navbar navbar-dropdown navbar-expand-lg">
                <div class="container">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <img src="assets/images/fogo-brazilian-bbq-logo.jpg" alt="" style="height: 3rem;">
                        </span>
                        <span class="navbar-caption-wrap">
                            <a class="navbar-caption text-white display-7" href="#">Fogo Brazilian Barbeque - Grand Galaxy Park</a>
                        </span>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-bs-toggle="collapse" data-target="#navbarSupportedContent" data-bs-target="#navbarSupportedContent" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <div class="hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                            <li class="nav-item">
                                <a class="nav-link link text-white text-primary display-4" href="index.html#features10-5"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </section>
        <section data-bs-version="5.1" class="header13 cid-tM9JGrErxw mbr-parallax-background" id="header13-1">
            <div class="align-center container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-12"></div>
                </div>
            </div>
        </section>
        <section data-bs-version="5.1" class="content5 cid-tM9O2MyUhA" id="content5-3">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12 col-lg-4">
                        <h4 class="mbr-section-subtitle mbr-fonts-style mb-4 display-7">Reservasi lebih dari 10 pax, dikenakan DP terlebih dahulu, hubungi kami melalui&nbsp; <br>WhatApp ! </h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            <strong>Jika Anda belum tiba atau tidak dapat dihubungi melalui telepon dalam waktu 15 menit setelah waktu reservasi Anda, restoran berhak menandai reservasi Anda sebagai ketidak hadiran atau pembatalan. </strong>
                            <br>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section data-bs-version="5.1" class="content5 cid-tM9OI2aF21" id="content5-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12 col-lg-4">
                        <p class="mbr-text mbr-fonts-style display-7"> Tarif&nbsp; anak 62.500++/Pax &amp; berlaku di bawah 110 cm Lansia(&gt;65 tahun) harus menunjukkan&nbsp; KTP yang masih berlaku.</p>
                    </div>
                </div>
            </div>
        </section>
        <section data-bs-version="5.1" class="content5 cid-tM9Pi9857u" id="content5-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12 col-lg-4">
                        <p class="mbr-text mbr-fonts-style display-7">
                            <strong>KAMI TIDAK DAPAT MENJAMIN PERMINTAAN MEJA ATAU PERMINTAAN MEJA PANJANG, MEJA BERDASARKAN KETERSEDIAAN&nbsp;</strong>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section data-bs-version="5.1" class="content11 cid-tM9QesYs97" id="content11-6">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-12 col-lg-10">
                        <div class="mbr-section-btn align-center">
                            <a class="btn btn-primary display-4" onclick="hiddShow()">Confirm and Continue</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="pesanan">
            <section data-bs-version="5.1" class="form7 cid-tM9QQwTqjp" id="form7-7">
                <div class="container">
                    <div class="mbr-section-head">
                        <h3 class="mbr-section-title mbr-fonts-style align-center mb-0 display-2">
                            <strong>Fogo Brazilian Barbeque - Grand Galaxy Park</strong>
                        </h3>
                    </div>
                    <div class="row justify-content-center mt-4">
                        <div class="col-lg-8 mx-auto mbr-form">
                            <form>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-2 col-form-label">No Telepon</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="telepon" name="Telepon" placeholder="No Telepon">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="row">
                                        <div class="col">
                                            <label for="orang" class="col-sm-4 col-form-label"><strong>Orang</strong></label>
                                        </div>
                                        <div class="col">
                                            <label for="Tanggal" class="col-sm-4 col-form-label"><strong>Tanggal</strong></label>
                                        </div>
                                        <div class="col">
                                            <label for="Tanggal" class="col-sm-4 col-form-label"><strong>Waktu</strong></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label>Reguler</label>
                                            <input class="form-control" type="number" id="reguler" name="reguler" value="0" onchange="val_num()">
                                            <br>
                                            <label>Kids&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            <input class="form-control" type="number" id="kids" name="kids" value="0" onchange="val_num()">
                                            <br>
                                            <label>Senior&nbsp;&nbsp;</label>
                                            <input class="form-control" type="number" id="senior" name="senior" value="0" onchange="val_num()">
                                            <br>
                                        </div>
                                        <div class="col">
                                             <br>
                                            <input type="date" class="form-control" id="tanggal" name="tanggal" placeholder="No Telepon">
                                        </div>
                                        <div class="col">
                                            <br>
                                            <input type="time" id="jam" class="form-control" name="jam" min="10:00" max="21:00" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-10 ">
                                            <div class="row">
                                                <div class="col"></div>
                                                <div class="col">
                                                    <button type="button" class="btn btn-success" onclick="simpan()">Simpan</button>
                                                </div>
                                                <div class="col"></div>
                                            </div>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/parallax/jarallax.js"></script>
        <script src="assets/smoothscroll/smooth-scroll.js"></script>
        <script src="assets/ytplayer/index.js"></script>
        <script src="assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="assets/theme/js/script.js"></script>
    </body>
</html>
<script>
    function hiddShow() {
        var x = document.getElementById("pesanan");
        x.style.display = "none";
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
    document.getElementById("pesanan").style.display = "none";



    function simpan() {
        var nama    =  document.getElementById("nama").value;
        var telepon =  document.getElementById("telepon").value;
        var reguler =  document.getElementById("reguler").value;
        var kids    =  document.getElementById("kids").value;
        var senior  =  document.getElementById("senior").value;
        var tanggal =  document.getElementById("tanggal").value;
        var jam     =  document.getElementById("jam").value;

        if(jam < '10:00' || jam > '21:00'){
            alert('Mohon Maaf Jam Sudah Tutup');
        }else{

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var Millisecond = new Date().getMilliseconds();
        var Detik = new Date().getSeconds();
        var Menit = new Date().getMinutes();

        var no_boking = yyyy+mm+dd+'G'+Menit+Detik+Millisecond;

        // console.log(nama);
        // console.log(telepon);
        // console.log(reguler);
        // console.log(kids);
        // console.log(senior);
        // console.log(tanggal);
        // console.log(jam);

        // console.log();

        var pesan = 
        `
Halo, Fogo Brazillian Barbeque!
---------------------------------
Saya ${nama} Sudah Melakukan Reservasi dengan kode Book *${no_boking}* sebagai berikut:
                    
            
DATA DIRI

Nama : ${nama}
No.Telepon :${telepon}
Tanggal : ${SetDate(tanggal)}
Jam : ${jam}

Pax ${reguler} Reguler ${senior} Senior ${kids} Kids

*Batas keterlambatan 15  menit*
*Diatas 10 pax wajib dp*
*waktu makan 60 menit*

Reguler = *148.500*
Senior 65 tahun keatas = *74.250*
kids dibawah tinggi 110 = *74.250*
(harga net)

Terima kasih 🙏

Salam hormat,
Fogo Brazilian Barbaque`;

        var encoded = encodeURI(pesan);

        window.open('https://api.whatsapp.com/send?phone=6285923132315&text='+encoded, '_blank');
   
        $.ajax({
          method: "POST",
          url: "<?=base_url('api/All_Access/insert_jadwal')?>",
          data: {no_booking:no_boking, nama : nama, telepon : telepon,tanggal : tanggal, reguler : reguler,senior : senior, kids : kids,jam:jam},
          cache: false,
          success: function(data) {alert(data);},
          error: function(xhr, status, error) {console.error(xhr);}

          });

    }
      
    }

    function val_num() {
        var regulerx =  document.getElementById("reguler").value;
        var kidsx    =  document.getElementById("kids").value;
        var seniorx  =  document.getElementById("senior").value;
        if(regulerx < 0 || kidsx < 0||seniorx < 0){
            alert('Jumlah Tidak Boleh < 0');
            document.getElementById("reguler").value ='0';
            document.getElementById("kids").value ='0';
            document.getElementById("senior").value ='0';
        }
    }


    const GetDate=(str)=>{
        let arr = str.split("-");
        let months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
        let month = (1+months.indexOf(arr[1].toLowerCase())).toString();
        if(month.length==1) month='0'+month;
        let year = '20' + parseInt(arr[2]);
        let day = parseInt(arr[0]);
        let result = year + '-' + month + '-' + ((day < 10 ) ? "0"+day : day);
        return result;
    }

    const SetDate=(date)=>{
        let monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        let dt1;
        let formattedDate1 = new Date(date);
        let d1 = formattedDate1.getDate();
        let m1 = monthNames[formattedDate1.getMonth()];
        let y1 = formattedDate1.getFullYear();
        dt1  = d1+'-'+m1+'-'+y1;
        return dt1;
    }
</script>