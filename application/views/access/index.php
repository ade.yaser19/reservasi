<?= $this->session->flashdata('message'); ?>
<div class="box">
    <div class="box">
        <div class="box-body">
            <a href="<?= base_url('access/add'); ?>" class="btn btn-sm btn-flat btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
        </div>
    </div>
    <div class="box-body">
        <table id="example1" class="table table-bordered">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th>Nama Hak Akses</th>
                    <th>Nama Menu</th>
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $x = 1;
                foreach ($access as $acc) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $x++; ?></td>
                        <td><?= $acc['role']; ?></td>
                        <td><?= $acc['title']; ?></td>
                        <td style="text-align: center;">
                            <a onclick="return confirm('Hapus ?' );" href="<?= base_url('access/delete/') . $acc['id']; ?>" class="btn btn-flat btn-xs  btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->