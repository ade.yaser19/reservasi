<header class="main-header" style="background-color: #111;">
    <!-- Logo -->
    <a href="#" class="logo" style="background-color:#111;">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini" style="color:white;"><b>GG</b>P</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg" style="color:white;"><b><?php $data = $this->db->get('pengaturan')->row_array();
        echo $data['nama_sistem']; ?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top bg-black ">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" >
                        <img src="<?= base_url('assets/img/profile/') . $img; ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs" style="color:white;"><?= $name; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= base_url('assets/img/profile/') . $img; ?>" class="img-circle" alt="User Image">

                            <p>
                                <?= $name; $new_date = date_format(date_create($date_created), 'Y-m-d');?>
                                <small>Member since <?=$new_date ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= base_url('profile'); ?>" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?= base_url('auth/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>