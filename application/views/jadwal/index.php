<style type="text/css">
.my-custom-scrollbar {
position: relative;
height: 450px;
overflow: auto;
}
.table-wrapper-scroll-y {
display: block;
}
</style>
<div class="row">
  <div class="col-sm-12">
    <div class="box">
      <div class="box-header" style="background-color:black; order-color: black;">
        <div class="row">
          <div class="col-sm-6">
            <div class="card">
              <div class="card-body">
                <table class="table" style="background-color:black; color: white; border-color: black;">
                  <tr>
                    <td><b>R</b><b id="reservasi"><?=$reservasi[0]['jumlah_booking']?></b></td>
                    <td><b class="glyphicon glyphicon-dashboard fa-1x"> R</b><b id="reservasi1"></b><?=$reservasi[0]['jumlah_booking']?></td>
                    <td><b class="glyphicon glyphicon-floppy-remove fa-1x>">R</b><b id="duduk"></b><?=$hilang[0]['jumlah_booking']?></td>
                  </tr>
                  <tr>
                    <td><b>O</b><b id="orang"><?=$orang[0]['orang']?></b></td>
                    <td><b id="orang1"><?=$orang[0]['orang']?></td>
                    <td><b id="orang2"><?=$juml_duduk[0]['orang']?></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card">
              <div class="card-body" >
                <table class="table" style="background-color:black; color: white; border-color: black;">
                  <tr>
                    <td style="width: 350px;">
                      <div class="box box-solid box-default" style="background-color:#685850;color: white;">
                        <i class="fa fa-file fa-2x" aria-hidden="true" style="margin: 10px"></i>
                      </div>
                    </td>
                    <td style="width: 30px;">
                      <i class='glyphicon glyphicon-adjust fa-2x'></i>
                    </td>
                    <td style="width: 30px;">
                      <i class='fa fa-sun-o fa-2x'></i>
                    </td>
                    <td style="width: 30px;">
                      <i class='fa fa-moon-o fa-2x'></i>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->

      <div class="box-body">
        <div class="table-wrapper-scroll-y my-custom-scrollbar table-responsive">
        <table border ="1px" style="overflow-x:auto; width:100%">
          <thead>
            <tr>
              <th><span class="glyphicon glyphicon-floppy-save fa-2x" style="align-center;margin:10px;"></span></th>
              <th><span class="glyphicon glyphicon-user fa-2x" style="align-center;margin:10px;"></span></th>
              <th></th>
  <!--             <th id="j1"><center>01:00</center></th>
              <th id="j2"><center>02:00</center></th>
              <th id="j3"><center>03:00</center></th>
              <th id="j4"><center>04:00</center></th>
              <th id="j5"><center>05:00</center></th>
              <th id="j6"><center>06:00</center></th>
              <th id="j7"><center>07:00</center></th>
              <th id="j8"><center>08:00</center></th>
              <th id="j9"><center>09:00</center></th> -->
              <th id="j10"><center>10:00</center></th>
              <th id="j11"><center>11:00</center></th>
              <th id="j12"><center>12:00</center></th>
              <th id="j13"><center>13:00</center></th>
              <th id="j14"><center>14:00</center></th>
              <th id="j15"><center>15:00</center></th>
              <th id="j16"><center>16:00</center></th>
              <th id="j17"><center>17:00</center></th>
              <th id="j18"><center>18:00</center></th>
              <th id="j19"><center>19:00</center></th>
              <th id="j20"><center>20:00</center></th>
              <th id="j21"><center>21:00</center></th>
<!--               <th id="j22"><center>22:00</center></th>
              <th id="j23"><center>23:00</center></th> -->
            </tr>
          </thead>
          <tbody> 
            <?php $x = 1;
              foreach ($meja_kursi as $v){?>
            <tr>
              <tr>
              <td id="km"><?=$v['id_meja_kursi']?></td>
              <td id="org"><?=$v['kapasitas_minimum']?> - <?=$v['kapasitas_makasimum']?></td>
              <td></td>
<!--               <td>
                <b id="jda<?=$v['id_meja_kursi']?>-1-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-1-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-1-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-1-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-2-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-2-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-2-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-2-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-3-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-3-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-3-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-3-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-4-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-4-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-4-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-4-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-5-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-5-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-5-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-5-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-6-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-6-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-6-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-6-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-7-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-7-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-7-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-7-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-8-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-8-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-8-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-8-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-9-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-9-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-9-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-9-60"></b>
              </td> -->
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-10-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-10-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-10-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-10-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-11-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-11-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-11-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-11-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-12-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-12-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-12-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-12-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-13-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-13-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-13-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-13-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-14-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-14-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-14-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-14-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-15-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-15-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-15-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-15-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-16-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-16-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-16-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-16-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-17-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-17-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-17-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-17-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-18-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-18-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-18-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-18-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-19-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-19-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-19-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-19-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-20-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-20-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-20-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-20-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-21-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-21-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-21-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-21-60"></b>
              </td>
           <!--    <td>
                <b id="jda<?=$v['id_meja_kursi']?>-22-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-22-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-22-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-22-60"></b>
              </td>
              <td>
                <b id="jda<?=$v['id_meja_kursi']?>-23-15"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-23-30"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-23-45"></b>
                <b id="jda<?=$v['id_meja_kursi']?>-23-60"></b>
              </td> -->
            </tr>
          <?php }?>
          </tbody>
        </table>
       </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:black; color:white;">
        <h3 class="modal-title" id="exampleModalLongTitle">Pelanggan</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="background-color:#685850;color: white;">
        <table style="width:450px;">
          <tr>
              <td>
               <strong><div id="nama"></div></strong>
              </td>
              <td>
                <strong><div id="status"></div></strong>
              </td>
            <td>
            <select name="status_id" id="status_id" style="width:145px;height: 30px;" onchange="set_status()">
            <option value="Dibatalkan" style="background-color: black;color: white;">Dibatalkan</option>

            <option value="Diterima" style="background-color: blue;">Diterima</option>

            <option value="Dikonfirmasi" style="background-color: green;">Dikonfirmasi</option>

            <option value="Duduk" style="background-color: skyblue;">Duduk</option>

            <option value="Sebagian Telah Tiba" style="background-color: purple;">Sebagian Telah Tiba</option>

            <option value="Sebagian Telah Duduk" style="background-color: greenyellow;">Sebagian Telah Duduk</option>

            <option value="Tidak Datang" style="background-color: goldenrod;">Tidak Datang</option>

            <option value="Tidak Di Hubungkan Meja" style="background-color: red;">Tidak Di Hubungkan Meja</option>

            <option value="DP" style="background-color: yellow;">DP</option>

            <option value="Dibatalkan" style="background-color: black; color: white;">Dibatalkan</option>

            <option value="Selesai" style="background-color: violet;">Selesai</option>
          </select>
         </td>
         </tr>
         <tr>
           <td>
             <strong><div id="telepon"></div></strong>
           </td>
         </tr>
         <tr>
          <td>
            <strong><div id="tanggal"></div></strong>
          </td>
         </tr>
         <tr>
          <td>
             <strong><div id="jam"></div></strong>
          </td>
         </tr>
         <tr>
          <td>
             <strong><div id="kd_meja"></div></strong>
             <input type="hidden" name="no_boking" id="no_boking">
          </td>
         </tr>
         </td>
        </table>
      </div>
      <div class="modal-footer" style="background-color:black;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="simpan()">Save changes</button>
      </div>
    </div>
  </div>
</div>

<script>
  $( document ).ready(function() {
 // dateload();
  datetable();
  });

    function datetable(){
    var date = document.getElementById("date").value ;

    $.ajax(
      {
         type: 'post',
         url: '<?=base_url('api/All_Access/get_datajadwal')?>',
         data: { 
           "date": date
         },
        success: function (response) {
         var data = JSON.parse(response);
         var style;
         var warna;
         var date;
         var warna_style ='';
         console.log(data);
          $.each(data, function(key,value) {
            console.log(value['Status']);
             if(value['Status']=='Diterima'){
               style ='';
               warna ='btn btn-primary';
             }else if(value['Status']=='Dikonfirmasi'){
               style ='';
               warna ='btn btn-success';
             }else if(value['Status']=='Duduk'){
              style ='';
              warna ='btn btn-info';
             }else if(value['Status']=='Sebagian Telah Tiba'){
              style ="style='background-color:#EA6CE8;'";
              warna_style ='#EA6CE8';
              warna ='btn';
             }else if(value['Status']=='Sebagian Telah Duduk'){
              style ='';
              warna ='btn btn-warning';
             }else if(value['Status']=='Tidak Datang'){
              warna ='btn';
              style = "style='background-color:goldenrod;'";
              warna_style ='goldenrod';
             }else if(value['Status']=='Tidak Di Hubungkan Meja'){
              style ='';
              warna ='btn btn-danger';
             }else if(value['Status']=='DP'){
              warna ='btn';
              style = "style='background-color:yellow;'";
              warna_style ='yellow';
             }else if(value['Status']=='Dibatalkan'){
              warna ='btn';
              style = "style='background-color:#A8A7AF;'";
              warna_style ='#A8A7AF';
             }else if(value['Status']=='Selesai'){
               warna ='btn';
               style = "style='background-color:violet;'";
               warna_style ='violet';
             }
             date = SetDate(value['tanggal']);
            var box = document.getElementById(value['id_urut']);

             box.innerHTML=`<button type="button" class="${warna}" ${style} Onclick="set_modal('${value['nama']}','${value['telepon']}','${date}','${value['jam']}','${value['id_meja_kursi']}','${value['kode_booking']}','${value['Status']}','${warna}','${warna_style}')">${value['nama']}</button>`;

          });
          //}
        },
         error: function () {
           alert("Error !!");
         }
      });
    }

    function set_modal(nama,telepon,tanggal,jam,kd_meja,kd_booking,status,warna,style) {
       document.getElementById('nama').innerHTML=`<span class="glyphicon glyphicon-user fa-1x">&emsp; ${nama}</span> `;

       document.getElementById('telepon').innerHTML=`<span class="glyphicon glyphicon-earphone fa-1x ">&emsp; ${telepon}</span> `;

        document.getElementById('tanggal').innerHTML=`<span class="glyphicon glyphicon-calendar fa-1x">&emsp; ${tanggal}</span> `;

        document.getElementById('jam').innerHTML=`<span class=" glyphicon glyphicon-dashboard fa-1x">&emsp; ${jam}</span> `;

        document.getElementById('kd_meja').innerHTML=`<span class="glyphicon glyphicon-floppy-remove fa-1x">&emsp; ${kd_meja}</span> `;

        console.log(style);
        document.getElementById('status').innerHTML=`
          <input type="button" id="status_button" value="${status}" class="${warna}" style="background-color:${style}">`;

       document.getElementById('no_boking').value=kd_booking;

       $("#exampleModalLong").modal('show');
 
    }

    function simpan() {
      var no_boking = document.getElementById('no_boking').value;
      var status    = document.getElementById('status_button').value;
       $.ajax({
          method: "POST",
          url: "<?=base_url('api/All_Access/update_jadwal_status')?>",
          data: {no_booking:no_boking,status:status},
          cache: false,
          success: function(data) {alert(data);
          window.location.replace("<?=base_url('jadwal')?>");
          },
          error: function(xhr, status, error) {console.error(xhr);}

          });
    }

    function set_status(){
      var status = document.getElementById("status_id").value;
      console.log(status);
      document.getElementById("status_button").value =status;
      
    }

  function dateload(){
    var date = document.getElementById("date").value ;
    window.location.replace("<?=base_url('jadwal/index')?>"+'/'+date);
    }

    const SetDate=(date)=>{
    let monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let dt1;
    let formattedDate1 = new Date(date);
    let d1 = formattedDate1.getDate();
    let m1 = monthNames[formattedDate1.getMonth()];
    let y1 = formattedDate1.getFullYear();
    dt1  = d1+'-'+m1+'-'+y1;
    return dt1;
  }

</script>