<?= $this->session->flashdata('message'); ?>
<div class="box">
    <div class="box">
        <div class="box-body">
            <a href="<?= base_url('menu/add'); ?>" class="btn btn-sm btn-flat btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
        </div>
    </div>
    <div class="box-body">
        <table id="example1" class="table table-bordered">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th>Nama Menu</th>
                    <th>Icon</th>
                    <th>Link</th>
                    <!-- <th>Status</th> -->
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $x = 1;
                foreach ($menu as $m) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $x++; ?></td>
                        <td><?= $m['title']; ?></td>
                        <td><?= $m['icon']; ?>-<i class="<?= $m['icon']; ?>"></i></td>
                        <td><?= $m['url']; ?></td>
                        <!-- <td><?= $m['is_active']; ?></td> -->
                        <td style="text-align: center;">
                            <a onclick="return alert('Anda yakin ingin merubah data ini ? ');"  href="<?= base_url('menu/edit/'.$m['id']) ?>" class="btn btn-flat btn-xs  btn-warning"><i class="glyphicon glyphicon-pencil"></i></a>
                            <a onclick="return alert('Anda yakin ingin menghapus data ini ? ');" href="<?= base_url('menu/delete/'.$m['id']) ?>" class="btn btn-flat btn-xs  btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->