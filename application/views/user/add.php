<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Tambah Pengguna</h3>
    </div>
    <div class="box-body">
        <form action="<?= base_url('users/add'); ?>" class="form" method="post">
            <div class="form-group">
                <label>Email</label>
                <input type="text" name="email" class="form-control" placeholder="Email">
            </div>
             <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" class="form-control" placeholder="Username">
            </div>
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap">
            </div>
            <div class="form-group">
                <label>No Telepon</label>
                <input type="text" name="no_telepon" class="form-control" placeholder="no_telepon">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password1" class="form-control" placeholder="Kata Sandi">
            </div>
            <div class="form-group">
                <label>Konfirmasi Password</label>
                <input type="text" name="password2" class="form-control" placeholder="Ulangi Kata Sandi">
            </div>
            <div class="form-group">
                <label>Hak Akses</label>
                <select name="role_id" class="form-control select2">
                  <option value="">- Pilih -</option>
                  <?php foreach ($role as $value) { ?>
                  <option value="<?php echo $value->id ?>">
                  <?php echo $value->role ?>
                  </option>
                  <?php } ?>
                </select>
            </div>
            <a href="<?= base_url('users') ?>" class="btn btn-sm btn-warning" style="float: right;  margin-left: 5px;">Kembali</a>
            <button class="btn btn-sm btn-primary" type="submit" style="float: right;">Simpan</button>
        </form>
    </div>
</div>