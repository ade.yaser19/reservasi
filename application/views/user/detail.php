<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Detail Pengguna</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>Nama</th>
                        <th><?= $name; ?></th>
                    </tr>
                    <tr>
                        <th>Username</th>
                        <th><?= $username; ?></th>
                    </tr>
                    <tr>
                        <th>Gambar</th>
                        <th> <img src="<?= base_url('assets/img/profile/') . $img; ?>"></th>
                    </tr>
                    <tr>
                        <th>Tanggal Daftar</th>
                        <th><?= date('Y-m-d', strtotime($date_created)); ?></th>
                    </tr>
                </table>
                <a href="<?= base_url('users') ?>" class="btn btn-sm btn-warning" style="float: right; margin: 5px;">Kembali</a>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>