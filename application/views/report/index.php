<?= $this->session->flashdata('message'); ?>
<div class="box">
    <div class="box-body">
        <input type='date' name='tanggal' id='tanggal' class="form-control" value='<?php echo $date = $tanggal !=''?$tanggal:date('Y-m-d')?>' min='1990-01-01' max='2040-12-31'
        onchange="load()">
    </div>
</div>
<div class="box">
    <div class="box-header" style="background-color:black;">
        <button type="button" class="btn" onclick="cetak_excel()">Excel</button>
        <button type="button" class="btn" onclick="cetak_pdf()">PDF</button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th style="text-align: center;">Tanggal</th>
                    <th style="text-align: center;">Jam</th>
                    <th style="text-align: center;">No. Meja</th>
                    <th style="text-align: center;">Nama</th>
                    <th style="text-align: center;">No. Telepon</th>
                    <th style="text-align: center;">Jumlah Pax</th>
                    <th style="text-align: center;">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $x = 1;
                $jumlah_pax='';
                foreach ($report as $r) : 
                 $jumlah_pax  = $r['reguler'] != 0 ? $r['reguler'].' &nbsp; Reguler &nbsp;':' &nbsp;';
                 $jumlah_pax .= $r['senior'] != 0 ? $r['senior'].' &nbsp; Senior &nbsp;':' &nbsp;';
                 $jumlah_pax .= $r['kids'] != 0 ? $r['kids'].' &nbsp; Kids &nbsp;':' &nbsp;';
                ?>
                    <tr>
                        <td style="text-align: center;"><?= $x++; ?></td>
                        <td><?= $r['tanggal']; ?></td>
                        <td><?= $r['jam']; ?></td>
                        <td><?= $r['id_meja_kursi']; ?></td>
                        <td><?= $r['nama']; ?></td>
                        <td><?= $r['telepon']; ?></td>
                        <td><?= $jumlah_pax; ?></td>
                        <td><?= $r['Status']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

<script type="text/javascript">
    function cetak_excel(){
        var tanggal = document.getElementById('tanggal').value;
        window.location.href = "<?=base_url('Report/cetak_excel')?>"+"/"+tanggal;
    }

    function cetak_pdf(){
         var tanggal = document.getElementById('tanggal').value;
        window.location.href = "<?=base_url('Report/cetak_pdf')?>"+"/"+tanggal;
    }

    function load(){
        var tanggal = document.getElementById('tanggal').value;
        window.location.href = "<?=base_url('Report/index')?>"+"/"+tanggal;
    }

</script>
