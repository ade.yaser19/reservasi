

<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>

<?= $this->session->flashdata('message'); ?>
<div class="row">
    <div class="col-sm-6">
        <div class="box">

            <div class="box-header col-sm-12" style="background-color:black;">
                <!-- <button type="button" class="btn btn-danger">X Bersihkan</button>
                <button type="button" class="btn " style="background-color: #5E36F5; color: white;">+ Pelanggan</button> -->
            </div>
            <div class="box-header col-sm-12" style="background-color:black;">
              <div class="box box-solid box-default" style="background-color:#685850;color: white;">
                   <input type="text" name="namadepan" id="namadepan" placeholder="Nama Depan"
                   style="margin: 2px; width:230px;">
                   <input type="text" name="namabelakang" id="namabelakang" placeholder="Nama Belakang" style="margin: 2px; width:274px;">
                   <input type="text" name="no_telepon" id="no_telepon" placeholder="No Telepon" style="margin: 2px; width:510px;">
                </div>
            </div>

            <div class="box-header col-sm-7" style="background-color:black;">
                <input type='date' name='tanggal' id='tanggal' class="form-control" value='<?php echo date('Y-m-d')?>' min='1990-01-01' max='2040-12-31'>
                <br>
                <input type="time" id="jam" name="jam" min="00:00" max="23:59" style="width:120px" />

            <!--    <button type="button" style="margin: 2px; background-color:gray; width: 110px;" onclick="set_jam()">1j(Otomatis)</button>
                <input type="hidden" name="autojam" id="autojam" name="autojam"> -->
            </div>
            <div class="box-header col-sm-5" style="background-color:black;">
              <div class="box box-solid box-default" style="background-color:#685850;color: white;">
                    <i class="fa fa-file fa-1x" aria-hidden="true" style="margin: 10px;" data-toggle="modal" data-target="#exampleModal"> Noted DP</i> 

                <select name="status" id="status" style="width:130px;height: 30px;">
                    <option value="Dibatalkan" style="background-color: black;">Dibatalkan</option>
                    <option value="Diterima" style="background-color: blue;">Diterima</option>
                    <option value="Dikonfirmasi" style="background-color: green;">Dikonfirmasi</option>
                    <option value="Duduk" style="background-color: skyblue;">Duduk</option>
                    <option value="Sebagian Telah Tiba" style="background-color: purple;">Sebagian Telah Tiba</option>
                    <option value="Sebagian Telah Duduk" style="background-color: greenyellow;">Sebagian Telah Duduk</option>
                    <option value="Tidak Datang" style="background-color: goldenrod;">Tidak Datang</option>
                    <option value="Tidak Di Hubungkan Meja" style="background-color: red;">Tidak Di Hubungkan Meja</option>
                    <option value="DP" style="background-color: yellow;">DP</option>
                    <option value="Dibatalkan" style="background-color: black; color: white;">Dibatalkan</option>
                    <option value="Selesai" style="background-color: violet;">Selesai</option>
                </select> 
                </div>

                <div class="box box-solid box-default" style="background-color:#685850;color: white;">
                    
                     <label>Reguler</label>
                      <input type="number" id="reguler" name="reguler" value="0" onchange="val_num()">
                      <br>
                      <label>Kids&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                       <input type="number" id="kids" name="kids" value="0" onchange="val_num()">
                        <br>
                      <label>Senior&nbsp;&nbsp;</label>
                      <input type="number" id="senior" name="senior" value="0" onchange="val_num()">
                </div>
               
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            </div>
            <!-- /.box-body -->
        </div>


        <!-- /.box -->
    </div>

    <div class="col-sm-6">
        <div class="box">
            <div class="box-header" style="background-color:black; color: white;">
                <button type="button" class="btn btn-danger" onclick="batal()">X Batal</button>
                <button type="button" class="btn btn-success" onclick="simpan()">Simpan</button>
                <h3>Reservasi Baru</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <input type="hidden" id="no_boking1" name="no_boking1" value="-" />

                <div class="box-header col-sm-5" style="background-color:black;">
                <input type='date' name='date' id='date' class="form-control" value='<?php echo date('Y-m-d')?>' min='1990-01-01' max='2040-12-31'>
                <br>
                <input type="time" id="jam1" name="jam1" min="00:00" max="23:59" style="width:120px;background-color: red; color:white;" />

         <!--       <button type="button" style="margin: 2px; background-color:gray; width: 100px;" onclick="set_jam()">1j(Otomatis)</button>
                <input type="hidden" name="autojam" id="autojam" name="autojam"> -->
                <select name="orang_pesanan" id="orang_pesanan" style="width:220px;background-color: red; color: white;" onchange="set_val_pesanan()">
                    <option>Pelanggan</option>
                <?php foreach($pesanan as $pesanan){?>
                    <option value="|<?php echo $pesanan['nama']?>|<?php echo $pesanan['kode_booking']?>|<?php echo $pesanan['telepon']?>|<?php echo $pesanan['reguler']?>|<?php echo $pesanan['senior']?>|<?php echo $pesanan['kids']?>|<?php echo $pesanan['jam']?>|<?php echo $pesanan['tanggal']?>"><?php echo $pesanan['nama']?>
                    |<?php echo $pesanan['kode_booking']?></option>
                <?php }?>
                </select>

                <input type="text" id="jumlah_orang" name="jumlah_orang" style="width:220px;background-color: red; color: white;" />

                <select name="meja" id="meja" style="width:220px;background-color: red; color: white;">
                <?php foreach($meja as $meja){?>
                    <option value="<?php echo $meja['id_meja_kursi']?>"><?php echo $meja['id_meja_kursi']?></option>
                <?php }?>
                </select>

            </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="color:black;">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Memo Harian</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="form-group">
        <label for="exampleFormControlSelect2">No Booking</label>
        <select class="form-control" id="no_boking">
        <?php 
        foreach($booking_note as $val){?>
          <option value="<?php echo $val['kode_booking']?>"><?php echo $val['kode_booking']?>|| <?php echo $val['nama']?></option>
        <?php }?>
        </select>
      <div class="form-group">
        <label for="exampleFormControlTextarea1">Catatan</label>
        <textarea class="form-control" id="Catatan" rows="3"></textarea>   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="simpan_memo()">Save changes</button>
      </div>
    </div>
  </div>
</div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

<script type="text/javascript">
     function simpan_memo(){
    var no_booking = document.getElementById("no_boking").value ;
    var Catatan = document.getElementById("Catatan").value ;
    console.log(Catatan)

    $.ajax(
      {
         type: 'post',
         url: '<?=base_url('api/All_Access/save_dataDb')?>',
          data: {no_booking:no_booking, Catatan : Catatan},
          cache: false,
        success: function (response) {
 
         alert("Data Berhasil Disimpan");
        
        },
         error: function () {
          // alert("Error !!");
         }
      });

    document.getElementById('exampleModal').click();
    }

 function set_jam(){
    document.getElementById('autojam').value='1';
 }

 function simpan() {
        var nama    =  document.getElementById("namadepan").value +  document.getElementById("namabelakang").value;
        var telepon =  document.getElementById("no_telepon").value;
        var reguler =  document.getElementById("reguler").value;
        var kids    =  document.getElementById("kids").value;
        var senior  =  document.getElementById("senior").value;
        var tanggal =  document.getElementById("tanggal").value;
        var status  =  document.getElementById("status").value;
        var meja    =  document.getElementById("meja").value;
        var jam     =  document.getElementById("jam").value;

        var no_booking1   =  document.getElementById("no_boking1").value;

        

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var Millisecond = new Date().getMilliseconds();
        var Detik = new Date().getSeconds();
        var Menit = new Date().getMinutes();

        var no_boking = yyyy+mm+dd+'G'+Menit+Detik+Millisecond;
         
       // document.getElementById('no_boking').value=no_boking;
        if(no_booking1 != '-'){
            no_boking =no_booking1;
        }

        $.ajax({
          method: "POST",
          url: "<?=base_url('api/All_Access/insert_jadwal')?>",
          data: {no_booking:no_boking, nama : nama, telepon : telepon,tanggal : tanggal, reguler : reguler,senior : senior, kids : kids,jam:jam,status:status,meja:meja},
          cache: false,
          success: function(data) {alert(data);
          document.getElementById("namadepan").value ='';
          document.getElementById("namabelakang").value='';
          document.getElementById("no_telepon").value='';
          document.getElementById("reguler").value='';
          document.getElementById("kids").value='';
          document.getElementById("senior").value='';
          document.getElementById("status").value='';
          document.getElementById("jam").value='';

          },
          error: function(xhr, status, error) {console.error(xhr);}

          });
   }

    function val_num() {
        var regulerx =  document.getElementById("reguler").value;
        var kidsx    =  document.getElementById("kids").value;
        var seniorx  =  document.getElementById("senior").value;
        if(regulerx < 0 || kidsx < 0||seniorx < 0){
            alert('Jumlah Tidak Boleh < 0');
            document.getElementById("reguler").value ='0';
            document.getElementById("kids").value ='0';
            document.getElementById("senior").value ='0';
        }
    }

   function set_val_pesanan(){
    var store = document.getElementById("orang_pesanan").value;

    var mystore = store.split("|");
    var jumlah =parseInt(mystore[4])+parseInt(mystore[5])+parseInt(mystore[6]);
     document.getElementById("namadepan").value =mystore[1];
     document.getElementById("no_boking1").value =mystore[2];
     document.getElementById("no_telepon").value =mystore[3];
     document.getElementById("reguler").value =mystore[4];
     document.getElementById("senior").value =mystore[5];
     document.getElementById("kids").value =mystore[6];
     document.getElementById("tanggal").value =mystore[8];
     document.getElementById("jam").value =mystore[7];
     document.getElementById("jam1").value =mystore[7];
     document.getElementById("date").value =mystore[8];
     document.getElementById("jumlah_orang").value =jumlah;
   }

   function batal(){
    window.location.replace("<?=base_url('pesanan')?>");
   }
  
   
</script>