<div class="box" style="background-color: #111;">
    <div class="box-body"> Selamat Datang, <?= $name; ?> </div>
    <!-- /.box-body -->
    <div class="row">
        <div class="col-sm-12 border-right-0">
            <div class="card">
                <h5 class="card-title">Memo Harian</h5>
                <div class="box box-solid box-default" style="background-color:#685850;color: white;">
                    <i class="fa fa-file fa-2x" aria-hidden="true" style="margin: 10px;" data-toggle="modal" data-target="#exampleModal"></i>  
                    <br><br><br><br><br>
                </div>
            </div>
        </div>
    </div>
    <table>
        <tr>
            <td><i class='glyphicon glyphicon-adjust fa-2x'></i></td>
            <td><i class='fa fa-sun-o fa-2x'></i></td>
            <td><i class='fa fa-moon-o fa-2x'></i></td>
        </tr>
       <tr>
            <td> Sepanjang Hari</td>
            <td><span style="margin:5px">Makan Siang</span></td>
            <td><span style="margin:5px">Makan Malam</span> </td>
        </tr>

    </table>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="box box-solid box-default" style="background-color:#685850;color: white;">
                   <table border="1px">
                       <tr>
                        <td><b id="reservasi">11</b></td>
                        <td><b id="orang">11</b></td>
                        <td><b id="order">RP 0</b></td>
                        <td><b class="glyphicon glyphicon-floppy-remove fa-1x" style="margin:2px; background-color:red;" id="hilang"></i> ? 0</td>
                       </tr>
                        <tr>
                        <td><strong>RESERVASI</strong></td>
                        <td><strong>ORANG</strong></td>
                        <td><strong>PREORDER</strong></td>
                        <td><strong>TIDAK DAPAT DI HUBUNGI</strong></td>
                       </tr>
                   </table>
                   <br><br><br><br><br>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="color:black;">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Memo Harian</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="form-group">
        <label for="exampleFormControlSelect2">No Booking</label>
        <select class="form-control" id="no_boking">
        <?php foreach($rest_combo as $val){?>
          <option value="<?php echo $val['kode_booking']?>"><?php echo $val['kode_booking']?></option>
        <?php }?>
        </select>
      </div>
      <div class="form-group">
        <label for="exampleFormControlTextarea1">Catatan</label>
        <textarea class="form-control" id="Catatan" rows="3"></textarea>   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="simpan()">Save changes</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $( document ).ready(function() {
      dateload();
  });


   function dateload(){
    var date = document.getElementById("date").value ;

    $.ajax(
      {
         type: 'post',
         url: '<?=base_url('api/All_Access/get_dataDb')?>',
         data: { 
           "date": date
         },
        success: function (response) {
           var json = JSON.parse(response);
           console.log(json);
           var orang = (json.orang[0].orang != null) ? json.orang[0].orang:'0';
           document.getElementById("reservasi").innerHTML =json.reservasi[0].jumlah_booking;
           document.getElementById("orang").innerHTML = orang;
           document.getElementById("hilang").innerHTML ='  ? '+json.hilang[0].jumlah_booking;
        },
         error: function () {
           alert("Error !!");
         }
      });
    }

   function simpan(){
    var no_booking = document.getElementById("no_boking").value ;
    var Catatan = document.getElementById("Catatan").value ;
    console.log(Catatan)

    $.ajax(
      {
         type: 'post',
         url: '<?=base_url('api/All_Access/save_dataDb')?>',
          data: {no_booking:no_booking, Catatan : Catatan},
          cache: false,
        success: function (response) {
 
         alert("Data Berhasil Disimpan");
        
        },
         error: function () {
          // alert("Error !!");
         }
      });

    document.getElementById('exampleModal').click();
    }
   
</script>