<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('User_model', 'user');
    }

    public function role()
    {
        $this->db->select('*');
        $this->db->from('role');
        return $this->db->get()->result_object();
    }

    public function index()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'head'          => 'Pengguna',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['user'] = $this->db->get('users')->result_array();

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('user/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $role   = $this->role();
        $data = [
            'title'         => 'Pengguna',
            'head'          => 'Pengguna',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
            'role'          => $role
        ];

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required', [
            'required' => 'Nama anda tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[5]', [
            'required'      => 'Kata sandi tidak boleh kosong',
            'min_length'    => 'Kata sandi anda terlalu pendek'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'trim|matches[password1]', [
            'matches'      =>  'Kata sandi tidak sama!'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('user/add');
            $this->load->view('templates/footer');
        } else {
            $data = [
                'role_id'       => $this->input->post('role_id'),
                'nama'          => htmlspecialchars($this->input->post('nama'), TRUE),
                'username'      => htmlspecialchars($this->input->post('username'), TRUE),
                'password'      => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'img'           => 'default.png',
                'date_created'  => time(),
                'password_hint' =>$this->input->post('password1'),
                'email'         =>$this->input->post('email'),
                'no_telepon'    =>$this->input->post('no_telepon')

            ];

            $this->db->insert('users', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil disimpan
                </div>');
            redirect('users');
        }
    }

    public function edit()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('role_id', 'Role', 'trim|required', [
            'required' => 'Role tidak boleh kosong'
        ]);
        $this->form_validation->set_rules('name', 'Nama', 'trim|required', [
            'required' => 'Nama tidak boleh kosong'
        ]);
        if ($this->form_validation->run() == false) {
            $name = $user['nama'];
            $img  = $user['img'];
            $date_created = $user['date_created'];
            $role   = $this->role();
            $data = [
                'title'         => 'Pengguna',
                'head'          => 'Pengguna',
                'name'          => $name,
                'img'           => $img,
                'date_created'  => $date_created,
                'role'          => $role
            ];
            $get = $this->user->detail();
            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('user/edit', $get);
            $this->load->view('templates/footer');
        } else {
            $id = $this->input->post('id');
            $data = [
                'role_id' => htmlspecialchars($this->input->post('role_id'), TRUE),
                'nama' => htmlspecialchars($this->input->post('name'), TRUE),
                'username' => htmlspecialchars($this->input->post('username'), TRUE),
                'password'      => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            ];
            $this->user->update($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil diubah
                </div>');
            redirect('users');
        }
    }

    public function detail()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'title'         => 'Pengguna',
            'head'          => 'Pengguna',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];
        $get = $this->user->detail();
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('user/detail', $get);
        $this->load->view('templates/footer');
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->user->hapus();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil dihapus
            </div>');
        redirect('users');
    }
}
