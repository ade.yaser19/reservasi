<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Pesanan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('api', 'api');
    }

    public function index()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();

        $this->db->order_by('priority', 'asc');
        $meja = $this->db->get('mst_meja_kursi')->result_array();
        $pesanan = $this->api->pelanggan_bokking();
        $booking_note = $this->api->pelanggan_bokking_note(date("Y-m-d"));

        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'head'          => 'Pesanan & Pelanggan',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
            'meja'          => $meja,
            'pesanan'       => $pesanan,
            'booking_note'  => $booking_note
        ];


        $this->load->view('templates/head');
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('pesanan/index', $data);
        $this->load->view('templates/footer');
    }
}
