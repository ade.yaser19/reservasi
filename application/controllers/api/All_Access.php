<?php
defined('BASEPATH') or exit('No direct script access allowed');

class All_Access extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api', 'api');
    }

    public function insert_jadwal()
    {

        $cek_no_booking = $this->api->cek_kode_booking($_POST['no_booking']);
        $no_booking = $_POST['no_booking'];
        if($cek_no_booking['jumlah']==0){

            $post_data = array(
                'kode_booking'=>$no_booking,
                'nama'=>$_POST['nama'],
                'tanggal'=>$_POST['tanggal'],
                'telepon'=>$_POST['telepon'],
                'reguler'=>$_POST['reguler'],
                'senior'=>$_POST['senior'],
                'kids'=>$_POST['kids'],
                'jam'=>$_POST['jam'],
                'Status'=>$this->input->post('status') ? $this->input->post('status'):'Baru',
                'id_meja_kursi'=>$this->input->post('meja'),
            );
            $insert_jadwal = $this->api->insert_jadwal($post_data);

            if($insert_jadwal){
                 echo json_encode('Data Berhasil Di Simpan...!');
            }
        }else{
              $post_data = array(
                'nama'=>$_POST['nama'],
                'tanggal'=>$_POST['tanggal'],
                'telepon'=>$_POST['telepon'],
                'reguler'=>$_POST['reguler'],
                'senior'=>$_POST['senior'],
                'kids'=>$_POST['kids'],
                'jam'=>$_POST['jam'],
                'Status'=>$this->input->post('status') ? $this->input->post('status'):'Baru',
                'id_meja_kursi'=>$this->input->post('meja'),
            );
            $update_jadwal = $this->api->update_jadwal($post_data,$no_booking);
            echo json_encode('Data Berhasil Di Simpan...!');
        }

    }

    public function update_jadwal_status()
    {
        $no_booking = $_POST['no_booking'];
        $post_data = array(
            'Status'=>$this->input->post('status') ? $this->input->post('status'):'Baru',
        );

        $update = $this->api->update_jadwal($post_data,$no_booking);

        if($update){
            echo json_encode('Data Berhasil Di Simpan...!');
        }

    }

    public function send_password()
    {
        $data['nama'] = $this->db->get('pengaturan')->row_array();
        $this->load->view('templates/auth_head', $data);
        $this->load->view('page/lupa_password');
        $this->load->view('templates/auth_foot');
    }

    public function get_dataDb()
    {

       $post_data= $_POST['date'];
       $data['reservasi'] = $this->api->data_reservasi($post_data);
       $data['orang'] = $this->api->data_orang($post_data);
       $data['hilang'] = $this->api->data_cancel($post_data);
       $data['duduk'] = $this->api->data_duduk($post_data);
       $data['juml_duduk'] = $this->api->j_data_duduk($post_data);
       echo json_encode($data);
    }

    public function save_dataDb(){

        return $this->api->save_memo($_POST);
    }

    public function get_password()
    {
        $rest_password = $this->api->get_pass($_POST['username'],$_POST['telepon']);

        $no_telp =$rest_password['no_telepon'];
        $password= $rest_password['password_hint'];

        $kirm =$this->kirim_WA($password,$no_telp);

        if($kirm){
             $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
               Password Kamu Sudah Berhasil Terkirim Ke No Wa...!
                </div>');
            redirect('auth');
        }
    }

    public function kirim_WA($password='',$no_telp='')
    {
       $api_key   = 'ea6922e884d6be62cf3c8adff2ef424e94da89cc'; // API KEY Anda
        $id_device = '085923132315'; // ID DEVICE yang di SCAN (Sebagai pengirim)
        $url   = 'https://wa.srv9.wapanels.com/send-message'; // URL API
        $no_hp = $no_telp; // No.HP yang dikirim (No.HP Penerima)
        $pesan = "Password kamu : $password  Terimakasih 🙏"; // Pesan yang dikirim
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 0); // batas waktu response
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        
        $data_post = [
           'api_key' => $api_key,
           'sender'  => $id_device,
           'number'  => $no_hp,
           'message' => $pesan
        ];

        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data_post));
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    public function get_datajadwal()
    {
       $post_data= $_POST['date'];
       $data = $this->api->data_jadwal($post_data);
       echo json_encode($data); 
    }
        

}
