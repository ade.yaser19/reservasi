<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('api', 'api');
    }

    public function index()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $date           =  date("Y-m-d");

        $rest_combo     = $this->api->get_booking($date);
        $data = [
            'head'          => "<input type='date' name='date' id='date' onchange='dateload()' style='background-color:black; color:#ffff' value='$date' min='1990-01-01' max='2040-12-31'>",
            'name'          => $name,
            'img'           => $img,
            'role'          => $user['role_id'],
            'date_created'  => $date_created,
            'rest_combo'    =>$rest_combo
        ];

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('member/dashboard');
        $this->load->view('templates/footer');
    }
}
