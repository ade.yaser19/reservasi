<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        session();
        $this->load->model('menu_model', 'menu');
    }

    public function index()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => 'Menu',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['menu'] = $this->db->get('menu')->result_array();

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('menu/index');
        $this->load->view('templates/footer');
    }

    public function add()
    {   
        $post = $this->input->post();

        if ($post) {

            $data = [
                'title' => $post['title'],
                'icon' => $post['icon'],
                'url' => $post['url'],
            ];

            $this->menu->save($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            Hak akses Menu berhasil ditambah
            </div>');
            redirect('menu');
        
        } else {

            $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
            $name           = $user['nama'];
            $img            = $user['img'];
            $date_created   = $user['date_created'];
            $data = [
                'head'          => 'Hak akses Menu',
                'name'          => $name,
                'img'           => $img,
                'date_created'  => $date_created
            ];

            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('menu/add', $data);
            $this->load->view('templates/footer');
        }
    }

    public function edit($id = null)
    {

        $post = $this->input->post();
        if ($post) {
            $data = [
                'id' => $this->input->post('id'),
                'title' => $this->input->post('title'),
                'icon' => $this->input->post('icon'),
                'url' => $this->input->post('url')
            ];
            $this->menu->update($data, $this->input->post('id'));
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil diubah
                </div>');
            redirect('menu');

        } else {
            // $menu           = $this->db->get_where('menu', ['id' => $id])->row_array();
            $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
            $name           = $user['nama'];
            $img            = $user['img'];
            $date_created   = $user['date_created'];
            $data = [
                'head'          => 'Menu',
                'name'          => $name,
                'img'           => $img,
                'date_created'  => $date_created
            ];

            $data['menu'] = $this->db->get_where('menu', ['id' => $this->uri->segment(3)])->row_array();
            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('menu/edit');
            $this->load->view('templates/footer');

        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->delete('menu', ['id' => $id]);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            Menu berhasil dihapus
            </div>');
        redirect('menu'); 
    }
}
