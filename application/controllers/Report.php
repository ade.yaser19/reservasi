<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('report_model', 'report');
    }

    public function index()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];

       // echo $this->uri->segment(3);die;

        $tanggal = $this->uri->segment(3) !=''?$this->uri->segment(3):date('Y-m-d');

        $data = [
            'head'          => 'Reporting',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
            'tanggal'       => $tanggal
        ];

        $data['report'] = $this->report->reporting_view($tanggal);
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('report/index', $data);
        $this->load->view('templates/footer');
    }

    public function cetak_pdf()
    {
        $pdf = new TCPDF('P', 'mm','A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle('cetak Laporan');
        
        // set header and footer fonts
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        
        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        
        // set some language-dependent strings (optional)
        $pdf->SetFont('dejavusans', '', 10);

        $pdf->AddPage();

        $tanggal   = $this->uri->segment(3);
        $report = $this->report->reporting_view($tanggal);

            $html ='<span style="text-align: center"><strong>Laporan Reservasi Fogo Brazillian Barbeque</strong></span>
            <br>
            <br>
            <table border="1">
              <tr>
                  <th style="text-align: center; width:25px">No</th>
                  <th style="text-align: center;">Tanggal</th>
                  <th style="text-align: center; width:40px">Jam</th>
                  <th style="text-align: center; width:55px">No.Meja</th>
                  <th style="text-align: center; width:120px">Nama</th>
                  <th style="text-align: center; width:100px">No. Telepon</th>
                  <th style="text-align: center; width:160px">Jumlah Pax</th>
                  <th style="text-align: center; width:60px">Status</th>
              </tr>
            ';

           $x = 1;
           $jumlah_pax='';
           foreach ($report as $r){
            $jumlah_pax  = $r['reguler'] != 0 ? $r['reguler'].' Reguler ':' &nbsp;';
            $jumlah_pax .= $r['senior'] != 0 ? $r['senior'].' Senior ':' &nbsp;';
            $jumlah_pax .= $r['kids'] != 0 ? $r['kids'].' Kids ':' &nbsp;';


            $html .='
              <tr>
                  <td style="text-align: center; width:25px">'.$x++.'</td>
                  <td style="text-align: center;">'.$r['tanggal'].'</td>
                  <td style="text-align: center; width:40px">'.$r['jam'].'</td>
                  <td style="text-align: center; width:55px">'.$r['id_meja_kursi'].'</td>
                  <td style="text-align: center; width:120px">'.$r['nama'].'</td>
                  <td style="text-align: center; width:100px">'.$r['telepon'].'</td>
                  <td style="text-align: center; width:160px">'.$jumlah_pax.'</td>
                  <td style="text-align: center; width:60px">'.$r['Status'].'</td>
              </tr>';
            } 
      
    $html .="</table>";

// output the HTML content
         $pdf->writeHTML($html, true, false, true, false, '');
         $pdf->Output('Reporting'.$tanggal.'.pdf', 'I');
    }


    public function cetak_excel()
    {
        $tanggal   = $this->uri->segment(3);
        $report = $this->report->reporting_view($tanggal);
        $table ='';

       header("Content-type: application/vnd-ms-excel");
       header("Content-Disposition: attachment; filename=Reporting $tanggal .xls");

      $table .='<center><strong>Laporan Reservasi Fogo Brazillian Barbeque</strong></center>';

      $table .= '<table border=1">
                <tr>
                    <th style="text-align: center;">No</th>
                    <th style="text-align: center;">Tanggal</th>
                    <th style="text-align: center;">Jam</th>
                    <th style="text-align: center;">No. Meja</th>
                    <th style="text-align: center;">Nama</th>
                    <th style="text-align: center;">No. Telepon</th>
                    <th style="text-align: center;">Jumlah Pax</th>
                    <th style="text-align: center;">Status</th>
                </tr>
           ';
                $x = 1;
                $jumlah_pax='';
                foreach ($report as $r){
                 $jumlah_pax  = $r['reguler'] != 0 ? $r['reguler'].' &nbsp; Reguler &nbsp;':' &nbsp;';
                 $jumlah_pax .= $r['senior'] != 0 ? $r['senior'].' &nbsp; Senior &nbsp;':' &nbsp;';
                 $jumlah_pax .= $r['kids'] != 0 ? $r['kids'].' &nbsp; Kids &nbsp;':' &nbsp;';

                $table .= '<tr>
                             <td style="text-align: center;">'.$x++.'</td>
                             <td>'.$r['tanggal'].'</td>
                             <td>'.$r['jam'].'</td>
                             <td>'.$r['id_meja_kursi'].'</td>
                             <td>'.$r['nama'].'</td>
                             <td>'.$r['telepon'].'</td>
                             <td>'.$jumlah_pax.'</td>
                             <td>'.$r['Status'].'</td>
                        </tr>';
                    }
         $table .="</table>";
         echo $table;

    }
}
